<?php
$Postcodes = file_get_contents('Postcodes.csv');
$Array = explode("\n", $Postcodes);
$Postcodes = utf8_encode($Postcodes);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>postcodes</title>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th>Postcode</th>
                <th>Stad</th>
                <th>District Nederlands</th>
                <th>District Frans</th>
            </tr>
        </thead>
        <tbody>
    <?php
    foreach ($Array as $row) {
        $Array = explode('|', $row);
        ?>
        <tr>
            <td><?php echo $Array[0];?></td>
            <td><?php echo $Array[1];?></td>
            <td><?php echo $Array[2];?></td>
            <td><?php echo $Array[3];?></td>
        </tr>
    <?php 
    } 
    ?>
    </tbody>
    </table>    
</body>
</html>