<?php


session_start();
if (empty($_SESSION['cart']))
{
    $_SESSION['cart'] = array();
}

array_push($_SESSION['cart'], $_GET['id']);

if (isset($_POST['Product'])) 
    {
        $Product = $_POST['Product'];
        switch ($Product) 
        {
            case 0 :
                $ProductText = 'Gitaar';
                break;
            case 1 :
                $ProductText = 'Drumstel';
                break;        
            case 2 :
                $ProductText = 'Elektrisch Drumstel';
                break;
            case 3 :
                $ProductText = 'Piano';
                break;
            case 4 :
                $ProductText = 'Blokfluit';
                break;        
            case 5 :
                $ProductText = 'Harmonica';
                break;
            case 6 :
                $ProductText = 'Bass';
                break;
            case 7 :
                $ProductText = 'Trompet';
                break;        
            case 8 :
                $ProductText = 'Accordion';
                break;
            case 9 :
                $ProductText = 'Klassieke Gitaar';
                break;
        }
    }
    
    if (isset($_POST['Aantal'])) 
    {
        $Aantal = $_POST['Aantal'];
        switch ($Aantal)
        {
            
        }
    }

?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <h1>Joris Bergmans Webwinkel</h1>
</head>
<body>
    
    <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post">
        <p>
    <label for="Aantal" maxlength="2">Aantal Producten</label>
    <input type="number" min="1" step="1">
        
        <select name ="Product" id ="Product">
            <option value="0">Gitaar</option>
            <option value="1">Drumstel</option>
            <option value="2">Elektronisch Drumstel</option>
            <option value="3">Piano</option>
            <option value="4">Blokfluit</option>
            <option value="5">Harmonica</option>
            <option value="6">Bass</option>
            <option value="7">Trompet</option>
            <option value="8">Accordion</option>
            <option value="9">Klassieke Gitaar</option>
        </select>
        <button type="submit">Verzenden</button> 
        </form>
        
        
         <div>
            <p><?php echo "Bestelde Producten {$Aantal}";?></p>
            <p><?php echo "Product Naam {$ProductText}";?></p>
            <a href="Webwinkel.php">Cart</a>

        </div>
</body>
</html>