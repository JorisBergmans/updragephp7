<?php

include ('vendor/modernways/dialog/src/Model/INotice.php');
include ('vendor/modernways/dialog/src/Model/Notice.php');
include ('vendor/modernways/dialog/src/Model/INoticeboard.php');
include ('vendor/modernways/dialog/src/Model/Noticeboard.php');


$nb = new \Modernways\Dialog\Model\Noticeboard();
$nb->startTimeInKey('Test Dialog');
$nb->setText('Mijn eerste foutmelding');
$nb->setCaption('dialog component testen');
$nb->setCode('001');
$nb->log();
$nb->startTimeInKey('Test Dialog 2 ');
$nb->setText('Mijn eerste foutmelding 2');
$nb->setCaption('dialog component testen 2');
$nb->setCode('002');
var_dump($nb);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>De Dialog Component</title>
</head>
<body>
    <pre><?php var_dump($nb);?></pre>
</body>
</html>