<?php
    $RekeningError = '';
    $PrijsError = '';
    if (isset($_POST['Rekening']))
    {
        $Rekeningnummer = $_POST['Rekening'];
        if (empty($Rekeningnummer))
        {
            $RekeningError = 'Rekeningnummer moet ingevuld zijn';
        }
    }
    
    if (isset($_POST['Plaats'])) 
    {
        $Plaats = $_POST['Plaats'];
        switch ($Plaats) 
        {
            case 0 :
                $categoryText = 'Staanplaats';
                break;
            case 1 :
                $categoryText = 'Zitplaats';
                break;        
            case 2 :
                $categoryText = 'Middenstuk';
                break;
        }
    }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Concert inschrijving</title>
    <h1>Bob Dylan</h1>
</head>
<body>
        
        <form action="<?php echo $_SERVER['PHP_SELF'];?>" method= "post">
            <label for="Voornaam" maxlength="2">Voornaam</label>
            <input id="Voornaam" name="Product" type= "text" size="20">
            <p>
            <label for="Naam" maxlength="2">Naam</label>
            <input id="Naam" name="Naam" type="text" size="20">
             <div>
                <label for="Plaats" maxlength="2">Plaats</label>
                <select name="Plaats" id="Plaats">
                    <option value="0">Staanplaats</option>
                    <option value="1">Zitplaats</option>
                    <option value="2">Middenstuk</option>
                </select>
                <p>
                <label for="Aantal" maxlength="2">Aantal Personen</label>
                <input type="number" min="1" step="1">
                <p>
                
                  <label>Birthday</label>
                  <label class="month">
                  <select class="select-style" name="BirthMonth">
                  <option value="">Month</option>
                  <option value="01">January</option>
                  <option value="02">February</option>
                  <option value="03" >March</option>
                  <option value="04">April</option>
                  <option value="05">May</option>
                  <option value="06">June</option>
                  <option value="07">July</option>
                  <option value="08">August</option>
                  <option value="09">September</option>
                  <option value="10">October</option>
                  <option value="11">November</option>
                  <option value="12" >December</option>
                  </label>
                 </select>   
                <label>Day<input class="birthday" maxlength="2" type ="number" min="1" step="1" name="BirthDay"  placeholder="Day" required=""></label>
                <label>Year <input class="birthyear" maxlength="4" type ="number" min="1" step="1"  name="BirthYear" placeholder="Year" required=""></label>
              
                <p>
                <label for="Rekeningnummer">Rekeningnummer</label>
                <input id="Rekeningnummer" name="Rekeningnummer" placeholder="Rekeningnummer" required>
                <span><?php echo $RekeningError;?></span>
                <p>
                <label for="Email">Email</label>
                <input id="email" name="email" placeholder="Voorbeeld@voorbeeld.com" required="" type="email">
                <p>
                <button type=submit>Verzenden</button>
            
            

        </form>
        
         <div>
            <p><?php echo "Je hebt gereserveerd voor {$Aantal}";?></p>
            <p><?php echo "Voor een {$categoryText}";?></p>

        </div>
        
</body>
</html>