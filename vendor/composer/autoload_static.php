<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitc4520738d7f17163c9e84835b4f64a73
{
    public static $prefixLengthsPsr4 = array (
        'M' => 
        array (
            'ModernWays\\Mvc\\' => 15,
            'ModernWays\\Dialog\\' => 18,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'ModernWays\\Mvc\\' => 
        array (
            0 => __DIR__ . '/..' . '/modernways/mvc/src',
        ),
        'ModernWays\\Dialog\\' => 
        array (
            0 => __DIR__ . '/..' . '/modernways/dialog/src',
        ),
    );

    public static $classMap = array (
        'EasyPeasyICS' => __DIR__ . '/..' . '/phpmailer/phpmailer/extras/EasyPeasyICS.php',
        'PHPMailer' => __DIR__ . '/..' . '/phpmailer/phpmailer/class.phpmailer.php',
        'PHPMailerOAuth' => __DIR__ . '/..' . '/phpmailer/phpmailer/class.phpmaileroauth.php',
        'PHPMailerOAuthGoogle' => __DIR__ . '/..' . '/phpmailer/phpmailer/class.phpmaileroauthgoogle.php',
        'POP3' => __DIR__ . '/..' . '/phpmailer/phpmailer/class.pop3.php',
        'SMTP' => __DIR__ . '/..' . '/phpmailer/phpmailer/class.smtp.php',
        'ntlm_sasl_client_class' => __DIR__ . '/..' . '/phpmailer/phpmailer/extras/ntlm_sasl_client.php',
        'phpmailerException' => __DIR__ . '/..' . '/phpmailer/phpmailer/class.phpmailer.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitc4520738d7f17163c9e84835b4f64a73::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitc4520738d7f17163c9e84835b4f64a73::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInitc4520738d7f17163c9e84835b4f64a73::$classMap;

        }, null, ClassLoader::class);
    }
}
