<?php

include(__DIR__ .'/vendor/autoload.php');
$appstate = new \ModernWays\Dialog\Model\NoticeBoard();
$request = new \ModernWays\Mvc\Request('/home/index');
$route = new \ModernWays\Mvc\Route($appstate, $request->uc());
$routeConfig = new \ModernWays\Mvc\RouteConfig('\Programmeren4\Gb', $route, $appstate);
$view = $routeConfig->invokeActionMethod();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <?php $view();?>
</body>
</html>
